# Instalacja dla administratorów serwerów #
1. Wrzuć z [paczki](https://bitbucket.org/sebek/amxx-team-balancer-manager/get/master.zip) cały folder `amxmodx` gdzieś na swój komputer.
2. Edytuj plik `amxmodx/scripting/kompilacja_amxx.bat` według własnych upodobań.
3. Odpal plik `amxmodx/scripting/kompilacja_amxx.bat` aby skompilować pliki `.sma`.
4. Cały folder `amxmodx` wrzuć na serwer do folderu `addons`.
5. Włącz plugin w pliku `amxmodx/configs/plugins.ini`.
