#include <amxmodx>

#define PLUGIN "TBM - Mod LVL"
#define VERSION "0.1"
#define AUTHOR "Sebul"

#pragma semicolon 1

//native dbm_get_user_level(id);
//native diablo_get_user_level(id);
native cod_get_user_level(id);

forward tbm_get_user_lvl(id);

public plugin_init() {
	register_plugin(PLUGIN, VERSION, AUTHOR);
}

public tbm_get_user_lvl(id) {
	return cod_get_user_level(id);
}
